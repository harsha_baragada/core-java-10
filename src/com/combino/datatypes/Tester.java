package com.combino.datatypes;

import com.combino.modifiers.ExampleAbstractClass;

public class Tester {
    public static void main(String[] args) {
        //Creating an object
        ExampleDataTypes dataTypes = new ExampleDataTypes();

        dataTypes.printDataTypes();
        System.out.println(ExampleAbstractClass.PI);
    }
}
