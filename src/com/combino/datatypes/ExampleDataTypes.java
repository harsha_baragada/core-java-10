package com.combino.datatypes;

public class ExampleDataTypes {

    public byte aByte = 127;
    private short aShort = 15455;
    private int anInt= 2147483647;
    private long aLong = 9223372036854575807L;

    private float aFloat = 4566.26f;
    private double aDouble = 565666.6642D;
    private char aChar = '@';
    private boolean aBoolean= true;


    public void printDataTypes(){
        System.out.println(aByte);
        System.out.println(aShort);
        System.out.println(anInt);
        System.out.println(aLong);

        System.out.println(aFloat);
        System.out.println(aDouble);
        System.out.println(aChar);
        System.out.println(aBoolean);
    }
}
