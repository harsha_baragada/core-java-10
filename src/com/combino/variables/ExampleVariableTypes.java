package com.combino.variables;

public class ExampleVariableTypes {

    //static variable
    public static final double PI = 3.14D;
    //global variable
    public double salary = 5000D;


    public ExampleVariableTypes() {
        // local variable
        byte b = 45;
    }

    public void sampleMethod() {

        int noOfDays = 20;
        System.out.println(noOfDays);


    }

    public void oneMoreMethod() {
        System.out.println(salary);
    }
}
