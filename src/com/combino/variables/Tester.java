package com.combino.variables;

public class Tester {
    public static void main(String[] args) {

        ExampleVariableTypes exampleVariableTypes = new ExampleVariableTypes();
        ExampleVariableTypes exampleVariableTypes1 = new ExampleVariableTypes();
        ExampleVariableTypes exampleVariableTypes2 = new ExampleVariableTypes();
        ExampleVariableTypes exampleVariableTypes3= new ExampleVariableTypes();

        exampleVariableTypes.sampleMethod();
        exampleVariableTypes.oneMoreMethod();
        System.out.println(exampleVariableTypes.salary);
        System.out.println(ExampleVariableTypes.PI);
    }
}
