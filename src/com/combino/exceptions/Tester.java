package com.combino.exceptions;

import java.io.FileNotFoundException;

public class Tester {
    public static void main(String[] args) throws FileNotFoundException {
        ExampleExceptions exampleExceptions = new ExampleExceptions();
        exampleExceptions.sampleMethodThatMayThrowException();
    }
}
