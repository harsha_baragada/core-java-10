package com.combino.exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExampleExceptions {

    public void readSomeFile() throws FileNotFoundException {

        File file = new File("C:\\Users\\VIHCADE\\Desktop\\sample.txt");
        FileReader fileReader = new FileReader(file);

    }

    public void unCheckedExceptions() throws IndexOutOfBoundsException {
        int[] ids = {1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009};

        System.out.println(ids[9]);
    }

    public void sampleMethodThatMayThrowException() {


        try {
            // all the business logic
            File file = new File("C:\\Users\\VIHCADE\\Desktop\\sample.txt");
            FileReader fileReader = new FileReader(file);
        } catch (IOException ioException) {
            System.out.println("The file you are trying to upload is not present at the location.!!");
        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {

        } finally {
            System.out.println("This is the finally block where this should be executed after all other blocks");
        }


    }

}
