package com.combino.constructors;

public class Tester {
    public static void main(String[] args) {
        ExampleConstructors objectWithDefaultConstructor = new ExampleConstructors();
        ExampleConstructors objectWithParameterizedConstructor = new ExampleConstructors(7777, "Peter parker");

        Person mark = new Person(1001, "Max mark");
        Person peter = new Person(1002, "Pater parker");
        Person yonus = new Person(1003, "yusuf yonus");
        Person oswald = new Person(1005, "Oswald");
        Person ram = new Person(9999, "Ram kumar");

        ram.getName(ram);
        ram.getId(ram);
    }
}
