package com.combino.constructors;

public class Person {
    private int id;
    private String name ;

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public void getName(Person person){
        System.out.println(person.name);
    }
    public void getId(Person person){
        System.out.println(person.id);
    }
}
