package com.combino.constructors;

public class ExampleConstructors {

    private int id;
    private String name;

    /*
    Constructor
    * */
    public ExampleConstructors() {
        id = 1001;
        System.out.println("This is a constructor from the ExampleConstructors class " + id);
    }

    public ExampleConstructors(int id, String name) {
        this.id = id;
        this.name = name;
        System.out.println("Your object has been created with id as " + id);
    }

    /*
        Method
        * */
    public float exampleMethod() {
        return 2.0f;
    }

}
