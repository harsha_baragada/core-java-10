package com.combino.oops.inheritance;

public class Tester {
    public static void main(String[] args) {

        Animal animal = new Mammal();
        Mammal mammal = new Mammal();
        Deer deer = new SpottedDear();
        Deer hornDeer = new HornDeer();
        Mammal mammal1 = new SpottedDear();

        mammal.printAnimal();
        deer.printAnimal();
        hornDeer.printAnimal();
        System.out.println();

        System.out.println(deer instanceof Animal);
        System.out.println(hornDeer instanceof Animal);
        System.out.println(mammal instanceof Animal);
        System.out.println(mammal1 instanceof Animal);
    }
}
