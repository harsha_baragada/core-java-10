package com.combino.oops.inheritance;

import java.util.Date;

public class Animal {

    private int id = 1001;

    public Animal(int id) {
        this.id = id;
        System.out.println("Ths is the parameterized constructor " + id);
    }

    public Animal() {
        System.out.println("This is the constructor from the animal class");
    }

    public void printAnimal() {
        System.out.println("This is the method from animal class");
        System.out.println(id);
    }

    private Date getDate() {
        return new Date();
    }
}
