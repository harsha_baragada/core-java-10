package com.combino.oops.inheritance;

public class Mammal extends Animal{

    public void printAnimal() {
        super.printAnimal();
        System.out.println("This is the method from mammal class");
    }
}
