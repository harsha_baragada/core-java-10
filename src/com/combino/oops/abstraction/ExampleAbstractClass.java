package com.combino.oops.abstraction;

public abstract class ExampleAbstractClass {

    public abstract String processAString();

    public void printSomething(){
        System.out.println("************************************");
    }

}
