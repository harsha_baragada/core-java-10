package com.combino.oops.abstraction;

public class Tester {
    public static void main(String[] args) {
        ExampleAbstractClass exampleAbstractClass = new AbstractImplementation();
        System.out.println(exampleAbstractClass.processAString());
        ExampleAbstractClass exampleAbstractClass1 = new AbstractImpl();
        System.out.println(exampleAbstractClass1.processAString());
    }
}
