package com.combino.oops.abstraction;

public class AbstractImplementation extends ExampleAbstractClass {
    @Override
    public String processAString() {
        return "Hello world!!";
    }
}
