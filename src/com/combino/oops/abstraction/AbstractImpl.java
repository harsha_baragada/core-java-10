package com.combino.oops.abstraction;

public class AbstractImpl extends ExampleAbstractClass {

    @Override
    public String processAString() {
        return "Ayesha implemented this method!!";
    }
}
