package com.combino.oops.interfaces;

public class Tester {

    public static void main(String[] args) {
        Person person = new PersonImpl();
        System.out.println(person.getPersonName());
        System.out.println(Person.exampleDefaultMethod());
    }
}
