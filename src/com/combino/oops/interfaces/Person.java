package com.combino.oops.interfaces;

public interface Person {

    double PI = 3.14;
    String CAPITAL_OF_INDIA = "New Delhi";


    String getPersonName();

    static String exampleDefaultMethod() {
        return "This is from interface";
    }
}
