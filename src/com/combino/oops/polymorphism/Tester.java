package com.combino.oops.polymorphism;

public class Tester {
    public static void main(String[] args) {
        Deer deer = new Deer();
        Animal animal = deer;
        Vegetarian  vegetarian = deer;
        Object o = deer;
    }
}
