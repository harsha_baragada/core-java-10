package com.combino.oops.encapsulation;

public class Tester {
    public static void main(String[] args) {
        Department department = new Department();
        department.setId(1001);
        department.setLocation("Newyork");
        department.setName("FED");
        department.setNoOfPeople(5000);


        System.out.println(department.getName());
        System.out.println(department);
        System.out.println( Integer.toHexString(department.hashCode()));

    }
}
