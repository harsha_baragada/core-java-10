package com.combino.oops.encapsulation;

public class Department {

    private int id;
    private String name;
    private int noOfPeople;
    private String location;


    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNoOfPeople() {
        return noOfPeople;
    }

    public void setNoOfPeople(int noOfPeople) {
        this.noOfPeople = noOfPeople;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Department*****************{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", noOfPeople=" + noOfPeople +
                ", location='" + location + '\'' +
                '}';
    }
}
