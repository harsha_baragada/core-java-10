package com.combino.oops.methodoverloading;

public class Person {

    public void run() {
        System.out.println("The person is running");
    }
}
