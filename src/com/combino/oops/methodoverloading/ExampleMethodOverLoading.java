package com.combino.oops.methodoverloading;

public class ExampleMethodOverLoading {

    public int sum(int a, int b) {
        return a + b;
    }

    public int sum(int a, int b, int c) {
        return a + b + c;
    }

    public double sum(double a, double b) {
        return a + b;
    }


}
