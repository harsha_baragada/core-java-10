package com.combino.oops.methodoverloading;

public class Tester {
    public static void main(String[] args) {
        int x = 10, y = 20, z = 50;
        double p = 20.22, q = 99.99;
        ExampleMethodOverLoading methodOverLoading = new ExampleMethodOverLoading();
        System.out.println(methodOverLoading.sum(x, y));
        System.out.println(methodOverLoading.sum(x, y, z));
        System.out.println(methodOverLoading.sum(p,q));
        System.out.println("**********************************");
        Person  person = new Person();
        person.run();

        Person ramu = new Ramu();
        ramu.run();

        Person somu =  new Somu();
        somu.run();


    }
}
