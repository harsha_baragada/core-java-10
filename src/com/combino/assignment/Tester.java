package com.combino.assignment;

public class Tester {
    public static void main(String[] args) {

        Person person= new Person();
        person.setAge(61);
        System.out.println(person.isSeniorCitizen(person.getAge()));
        Customer customer=new Customer();
        customer.setBalance(1000);
        customer.setDepositMoney(500);
        customer.setWithDrawal(500);
        System.out.println(customer.depositMoney(customer.getBalance(), customer.getDepositMoney()));
        System.out.println(customer.withDrawMoney(customer.getBalance(), customer.getWithDrawal()));

    }
}
