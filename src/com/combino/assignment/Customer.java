package com.combino.assignment;

public class Customer extends Person{
    private long accountNumber;
    private String address;
    private double balance;

    public double getWithDrawal() {
        return withDrawal;
    }

    public void setWithDrawal(double withDrawal) {
        this.withDrawal = withDrawal;
    }

    private double withDrawal;

    public double getDepositMoney() {
        return depositMoney;
    }

    public void setDepositMoney(double depositMoney) {
        this.depositMoney = depositMoney;
    }

    private double depositMoney;
    private boolean isAccountActive;

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean isAccountActive() {
        return isAccountActive;
    }

    public void setAccountActive(boolean accountActive) {
        isAccountActive = accountActive;
    }

    public String withDrawMoney(double balance, double withDrawal) {
        String result;
        if(balance>withDrawal)
        {
            result="Success";
        }
        else
        {
            result="failure";
        }
        return  result;
    }


    public double depositMoney(double balance, double depositAmount) {

        return balance+depositAmount;

    }
    }
