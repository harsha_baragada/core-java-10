package com.combino.generics;

import com.combino.constructors.Person;

public class Tester {
    public static void main(String[] args) {

        String[] cities = {"Mumbai", "Chennai", "Kolkata", "Hyderabad", "Chennai", "Bengaluru"};
        Integer[] ids = {1001, 1005, 1100558, 1025, 20032, 11546, 654654, 8468, 46854, 684, 684};
        Character[] chars = {'A', 'P', 'P', 'L', 'E'};

        ExampleGenericMethod genericMethod = new ExampleGenericMethod();
        genericMethod.printAnArray(cities);
        genericMethod.printAnArray(ids);
        genericMethod.printAnArray(chars);

        ExampleGenericClass<String> stringExampleGenericClass = new ExampleGenericClass<>();
        ExampleGenericClass<Integer> integerExampleGenericClass = new ExampleGenericClass<>();
        ExampleGenericClass<Person> personExampleGenericClass = new ExampleGenericClass<>();

        stringExampleGenericClass.setE("Hakuna matata");
        System.out.println(stringExampleGenericClass.getE());
    }
}
