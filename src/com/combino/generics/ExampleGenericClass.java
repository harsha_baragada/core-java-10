package com.combino.generics;

public class ExampleGenericClass<E> {

    private E e;


    public E getE() {
        return e;
    }

    public void setE(E e) {
        this.e = e;
    }
}
