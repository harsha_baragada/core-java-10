package com.combino.generics;

public class ExampleGenericMethod {

    public <E> void printAnArray(E[] items){
        for (E item:items ) {
            System.out.println(item);
        }
    }
}
