package com.combino.strings;

public class ExampleString {

    char[] fruit = {'A', 'P', 'P', 'L', 'E'};

    String string = new String();
    String string1 = new String("Eva");
    String name = "P=e=ter p=ark=er===";
    String fruitString = new String(fruit);
    String xxx = " ";

    String first = "Java";
    String second = "jAvA";

    public void playWithStrings() {

        System.out.println(name);
        System.out.println(string1);
        System.out.println(fruitString);
        System.out.println(fruitString.length());
        System.out.println(xxx.isEmpty());
        System.out.println(xxx.isBlank());
        System.out.println(fruitString.charAt(3));
        System.out.println(fruitString.codePointAt(3));
        System.out.println(fruitString.codePointBefore(3));
        System.out.println(first.equals(second));
        System.out.println(first.equalsIgnoreCase(second));

        // String concatination

        String s = first + " " + second;
        System.out.println(s);
        System.out.println(first.concat(" " + second));
        System.out.println(s.replace('a','y'));
        System.out.println(s.contains("jAvA"));
        System.out.println(name.replaceAll("=",""));
        System.out.println(String.join(",","jAvA"));

        xxx.strip();
        System.out.println(xxx.isEmpty());
        xxx.trim();
        System.out.println(xxx.isEmpty());

        String company = "vihcade";
        /*System.out.println(Character company.charAt(0));*/
        System.out.println(company.indexOf(76));

    }
}
