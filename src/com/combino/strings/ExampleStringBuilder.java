package com.combino.strings;

public class ExampleStringBuilder {

    public void printStringBuilder() {
        StringBuffer stringBuffer = new StringBuffer();
        StringBuilder stringBuilder = new StringBuilder("this is a builder this is a builder");

        System.out.println(stringBuilder);
        System.out.println(stringBuilder.capacity());
        System.out.println(stringBuilder.charAt(16));
        System.out.println(stringBuilder.substring(5,11));
        System.out.println(stringBuilder.reverse());
        System.out.println(stringBuilder);
    }
}
