package com.combino.loops;

public class ExampleForLoop {

    String[] cities = {"Mumbai", "New york", "London", "Amsterdam", "Paris","cairo"};

    /*
     * for (initialization; boolean_expression; update){
     * }
     *
     * */

    public void printForLoopUsage() {
        System.out.println(cities.length);
        for (int i = 0; i < cities.length; i++) {
            System.out.println(cities[i]);
        }

        System.out.println("Advance for loop");
        for (String city: cities
             ) {
            System.out.println(city);
        }
    }
}
