package com.combino.loops;

public class ExampleLoopControl {

    public void printLoopControls() {

        for (int i = 0; i < 20; i++) {
            if (i > 10) {
                break;
            }
            System.out.println(i);
        }

        System.out.println("##################################");
        for (int i = 0; i < 20; i++) {
            if (i == 10) {
                continue;
            }
            System.out.println(i);
        }
    }
}
