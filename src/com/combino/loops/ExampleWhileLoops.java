package com.combino.loops;

public class ExampleWhileLoops {

    int x = 40;

    public void printWhileLoop() {

        while (x > 20) {
            System.out.println(x);
            x--;
        }

        do {
            System.out.println("starting of do while");
            System.out.println(x);
            x--;
        }
        while (x > 10);
    }

}
