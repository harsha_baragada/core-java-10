package com.combino.wrapperclass;

public class ExampleNumbers {

    private int x = 10;
    private Number a = 10;
    private Integer p = 20;


    public  Byte aByte = 127;
    private Short aShort = 15455;
    private Long aLong = 9223372036854575807L;

    private Float aFloat = 4566.26f;
    private Double aDouble = 565666.6642D;
    private Character aChar = '@';
    private Boolean aBoolean= true;
    public void printExampleWrapperClasses() {

        System.out.println(a.byteValue());
        System.out.println(a.doubleValue());
        System.out.println(a.intValue());
        System.out.println(a.floatValue());
        System.out.println(a.longValue());
        System.out.println(a.shortValue());

        System.out.println(a.getClass());

        System.out.println(a.toString());

    }

    public void printWrapperClass(){
        int value=aDouble.intValue();
        Integer value1=value;
        System.out.println(value1.getClass());
        System.out.println(value1.toString());
        System.out.println(value1.toString().getClass());
    }
}
