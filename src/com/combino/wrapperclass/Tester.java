package com.combino.wrapperclass;

public class Tester {
    public static void main(String[] args) {
        ExampleNumbers exampleNumbers = new ExampleNumbers();
        exampleNumbers.printExampleWrapperClasses();
        exampleNumbers.printWrapperClass();
    }
}
