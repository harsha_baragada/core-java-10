package com.combino.modifiers;

public class ExampleAccessModifiers {

    //Visible to only package
    int x = 0;

    // visible to only class
    private int i = 10;

    // visible to the whole project
    public int s = 999;

    // visible to package along with subclasses
    protected int p = 5963;

    public void printPrivateVariable() {
        System.out.println(i);
    }

    public String sayName() {
        return "Peter parker";
    }

    public double getAge() {
        return 96.36;
    }

    public void thisMethodWillNotReturnAnything() {

    }

}
