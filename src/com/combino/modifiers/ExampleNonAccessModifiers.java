package com.combino.modifiers;

public  class ExampleNonAccessModifiers {

    public static int radius = 5066;

    public final int x = 10;

    public static void printSomething() {
        System.out.println("Printing something..........");
    }

    public final void someMethod(){
    }

}
