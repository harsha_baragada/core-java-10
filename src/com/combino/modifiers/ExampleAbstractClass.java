package com.combino.modifiers;

public abstract class ExampleAbstractClass {

    public static final double PI = 3.14;

    public abstract void exampleAbstractMethod();
}
