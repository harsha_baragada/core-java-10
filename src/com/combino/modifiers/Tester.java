package com.combino.modifiers;

public class Tester {

    public static void main(String[] args) {

        ExampleAccessModifiers modifiers = new ExampleAccessModifiers();
        System.out.println(modifiers.x);
        System.out.println(modifiers.p);
        System.out.println(modifiers.s);
        modifiers.printPrivateVariable();


        System.out.println(ExampleNonAccessModifiers.radius);
        ExampleNonAccessModifiers.printSomething();
    }
}
