package com.combino.operators;

public class Tester {
    public static void main(String[] args) {

        int x = 40, y = 20, ghdfhd = 520, iouyu = 89;
        double p = 36.66, q = 659.96;
        int a = 60, b = 13;

        ExampleArithmeticOperators arithmeticOperators = new ExampleArithmeticOperators();
        System.out.println("The addition of " + x + " and " + y + " is " + arithmeticOperators.sum(ghdfhd, iouyu));
        System.out.println("The subtraction of " + x + " and " + y + " is " + arithmeticOperators.subtraction(x, y));
        System.out.println("The multiplication of " + x + " and " + y + " is " + arithmeticOperators.multiplication(x, y));
        System.out.println("The division of " + x + " and " + y + " is " + arithmeticOperators.division(x, y));
        System.out.println("The modulus of " + x + " and " + y + " is " + arithmeticOperators.modulus(x, y));
        System.out.println("The increment of " + x + " is " + arithmeticOperators.increment(x));
        System.out.println("The decrement of " + x + " is " + arithmeticOperators.decrement(x));

        ExampleRelationalOperators relationalOperators = new ExampleRelationalOperators();
        System.out.println("The equals of " + x + " and " + y + " is " + relationalOperators.equals(x, y));
        System.out.println("The notEquals of " + x + " and " + y + " is " + relationalOperators.notEquals(x, y));
        System.out.println("The greaterThan of " + x + " and " + y + " is " + relationalOperators.greaterThan(x, y));
        System.out.println("The lessThan of " + x + " and " + y + " is " + relationalOperators.lessThan(x, y));
        System.out.println("The greaterThanOrEquals of " + x + " and " + y + " is " + relationalOperators.greaterThanOrEquals(x, y));
        System.out.println("The lessThanOrEquals of " + x + " and " + y + " is " + relationalOperators.lessThanOrEquals(x, y));
        relationalOperators.sampleVoidMethod(x, y);

        ExampleBitwiseOperators bitwiseOperators = new ExampleBitwiseOperators();
        System.out.println(bitwiseOperators.bitWiseAnd(a, b));
        System.out.println(bitwiseOperators.or(a, b));
        System.out.println(bitwiseOperators.compliment(a, b));
        System.out.println(bitwiseOperators.leftShift(a, b));
        System.out.println(bitwiseOperators.rightShift(a, b));

        System.out.println("############# logical operators ##############");
        ExampleLogicalOperators logicalOperators = new ExampleLogicalOperators();
        System.out.println(logicalOperators.and(true, false));
        System.out.println(logicalOperators.or(true, false));
        System.out.println(logicalOperators.logicalNot(true));
        System.out.println("############# assignment operators ##############");
        ExampleAssignmentOperators assignmentOperators = new ExampleAssignmentOperators();
        System.out.println(assignmentOperators.addAndAssign());
        System.out.println(assignmentOperators.subtractAndAssign());
        System.out.println(assignmentOperators.multiplyAndAssign());
        System.out.println(assignmentOperators.divideAndAssign());
        System.out.println(assignmentOperators.modulusAndAssign());
        System.out.println(assignmentOperators.addAndAssign());
        System.out.println(assignmentOperators.leftShift());

        System.out.println("*********** miscellaneous operator ***********");
        int year = 1998;
        String leapYear = (year % 4 == 0) ? "leap Year" : "non leap year"; // variable assignment = (expression) ? true: false;
        System.out.println(leapYear);


    }
}
