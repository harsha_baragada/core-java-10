package com.combino.operators;

public class ExampleAssignmentOperators {
    private int x = 10, A = 50;

    public int addAndAssign() {
        return A += x; // A = A+x
    }

    public int subtractAndAssign() {
        return A -= x; // A = A-x
    }

    public int multiplyAndAssign() {
        return A *= x; // A = A*x
    }

    public int divideAndAssign() {
        return A /= x; // A = A/x
    }

    public int modulusAndAssign() {
        return A %= x; // A = A%x
    }

    public int leftShift() {
        return A <<= 2; // A<< x
    }
}
