package com.combino.operators;

public class ExampleRelationalOperators {

    public boolean equals(int p, int q) {
        return p == q;
    }

    public boolean notEquals(int p, int q) {
        return p != q;
    }

    public boolean greaterThan(int p, int q) {
        return p > q;
    }

    public boolean lessThan(int p, int q) {
        return p < q;
    }

    public boolean greaterThanOrEquals(int p, int q) {
        return p >= q;
    }

    public boolean lessThanOrEquals(int p, int q) {
        return p <= q;
    }

    public void sampleVoidMethod(int p, int q) {
        System.out.println( p > q);
    }


}
