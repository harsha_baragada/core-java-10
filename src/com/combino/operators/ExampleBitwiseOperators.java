package com.combino.operators;

public class ExampleBitwiseOperators {

    public int bitWiseAnd(int a, int b) {
        return a & b;
    }

    public int or(int a, int b) {
        return a | b;
    }

    public int xor(int a, int b) {
        return a ^ b;
    }

    public int compliment(int a, int b) {
        return ~a;
    }

    public int leftShift(int a, int b) {
        return a<<2;
    }

    public int rightShift(int a, int b) {
        return a >> 2;
    }

}
