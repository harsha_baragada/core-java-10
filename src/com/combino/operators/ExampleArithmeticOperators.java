package com.combino.operators;

public class ExampleArithmeticOperators {

    public int sum(int a, int b) {
        return a + b;
    }

    public int subtraction(int a, int b) {
        return a - b;
    }

    public int multiplication(int a, int b) {
        return a * b;
    }

    public int division(int a, int b) {
        return a / b;
    }

    public int modulus(int a, int b) {
        return a % b;
    }

    public int increment(int a) {
        return ++a;
    }

    public int decrement(int a) {
        return --a;
    }

}
