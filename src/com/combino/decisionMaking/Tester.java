package com.combino.decisionMaking;

public class Tester {
    public static void main(String[] args) {
        ExampleDecisionMaking decisionMaking = new ExampleDecisionMaking();
        decisionMaking.printDecisionMaking();
        decisionMaking.printNestedIfElse();

        ExampleSwitchCase switchCase = new ExampleSwitchCase();
        switchCase.printSwitchCase();
        switchCase.exampleTerenaryOperator();
    }
}
