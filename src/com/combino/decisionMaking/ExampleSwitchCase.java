package com.combino.decisionMaking;

public class ExampleSwitchCase {
    public void printSwitchCase() {
        System.out.println("We are checking your grade and remarks");
        char grade = 'C';
        switch (grade) {
            case 'A':
                System.out.println("Excellent");
                break;
            case 'B':
                System.out.println("Very good");
                break;
            case 'C':
                System.out.println("Good");
                break;
            case 'D':
                System.out.println("Satisfactory");
                break;
            case 'E':
                System.out.println("Passed");
                break;
            case 'F':
                System.out.println("Failed");
                break;
            default:
                System.out.println("The grade assessment has been done");
        }
    }

    public void exampleTerenaryOperator() {
        int a = 5636898;
        String evenOrOdd = (a % 2 == 0) ? "Even" : "Odd";
        System.out.println("The number " + a + " is " + evenOrOdd);

    }
}
