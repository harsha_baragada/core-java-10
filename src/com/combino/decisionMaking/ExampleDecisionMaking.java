package com.combino.decisionMaking;

public class ExampleDecisionMaking {
    private double x = 9654536;

    String colorOfTheDog = "White";

    public void printDecisionMaking() {
        System.out.println("is the dog available");
        if (colorOfTheDog == "Black") {
            System.out.println("This dog has already been adopted");
        } else {
            System.out.println("The dog is available");
        }

    }

    public void printNestedIfElse() {

        if (x % 2 == 0) {
            System.out.println(x + " is divisible by 2");
            if (x % 3 == 0) {
                System.out.println(x + " is divisible by 2 & 3");
            } else {
                System.out.println(x + " is divisible by 2 but not 3");
            }
        } else {
            System.out.println(x + " is not divisible by 2");
        }


    }
}
