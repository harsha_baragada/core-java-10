package com.combino.collections.set;

public class Tester {
    public static void main(String[] args) {
        ExampleSet set = new ExampleSet();
        set.printSet();
        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        ExampleSortedSet sortedSet = new ExampleSortedSet();
        sortedSet.printSet();
    }
}
