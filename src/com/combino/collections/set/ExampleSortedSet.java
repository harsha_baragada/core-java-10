package com.combino.collections.set;

import java.util.*;

public class ExampleSortedSet {
    
    ArrayList<Integer> arrayList = new ArrayList<>();
    public void printSet(){

        arrayList.add(1001);
        arrayList.add(5999);
        arrayList.add(1001);
        arrayList.add(5999);
        arrayList.add(1001);
        arrayList.add(9696);
        arrayList.add(1001);
        arrayList.add(1003);
        arrayList.add(9696);
        arrayList.add(1001);
        arrayList.add(5999);
        arrayList.add(1001);
        arrayList.add(5999);
        arrayList.add(1001);
        arrayList.add(9696);
        arrayList.add(1001);
        arrayList.add(1003);
        arrayList.add(9696);

        System.out.println(arrayList);

        SortedSet<Integer> integerSortedSet = new TreeSet<>(arrayList);
        System.out.println(integerSortedSet);


    }
}
