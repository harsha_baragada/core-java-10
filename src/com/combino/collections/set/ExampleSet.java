package com.combino.collections.set;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class ExampleSet {

    Set<Integer>  integerSet = new HashSet<>();
    Collection<Integer> integers = new ArrayList<>();
    public void printSet(){

        integers.add(1003);
        integers.add(1004);
        integers.add(1005);
        integers.add(1006);

        integerSet.add(1001);
        integerSet.add(1002);
        integerSet.add(1003);
        integerSet.add(1004);
        integerSet.add(1005);
        integerSet.add(1006);
        integerSet.add(1007);
        integerSet.add(1008);
        System.out.println(integerSet);
        System.out.println(integerSet.retainAll(integers));
        System.out.println(integerSet);
        System.out.println(integerSet.removeAll(integers));
        System.out.println(integerSet);
    }
}
