package com.combino.collections;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ExampleCollections {

    Collection<String> fruitBasket = new ArrayList<>();
    /*AbstractCollection*/

    public void printFruitNames() {
        fruitBasket.add("Apple");
        fruitBasket.add("Banana");
        fruitBasket.add("Strawberry");
        fruitBasket.add("Blueberry");
        fruitBasket.add("Orange");
        fruitBasket.add("Avacado");

        System.out.println(fruitBasket);
        System.out.println(fruitBasket.size());
        System.out.println(fruitBasket.isEmpty());
        System.out.println(fruitBasket.contains("Orange"));

        Iterator<String>  fruitBasketIterator= fruitBasket.iterator();
        while (fruitBasketIterator.hasNext()){
            System.out.println(fruitBasketIterator.next());
        }

    }
}
