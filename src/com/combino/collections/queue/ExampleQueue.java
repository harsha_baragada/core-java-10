package com.combino.collections.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class ExampleQueue {

    Queue<Integer> queue = new PriorityQueue<>();

    public void printQueue() {

        queue.add(1001);
        queue.add(8964);
        queue.add(9999);
        queue.add(9632);
        queue.add(7854);
        queue.add(1240);
        queue.add(1002);
        System.out.println(queue);
        System.out.println(queue.peek());
        System.out.println(queue.contains(1002));
        System.out.println(queue.poll());
        System.out.println(queue);
        System.out.println(queue.poll());
        System.out.println(queue);


    }
}
