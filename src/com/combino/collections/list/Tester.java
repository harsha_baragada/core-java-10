package com.combino.collections.list;

public class Tester {
    public static void main(String[] args) {

        ExampleArrayList arrayList = new ExampleArrayList();
        arrayList.printArrayList();
        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        ExampleLinkedList linkedList = new ExampleLinkedList();
        linkedList.printLinkedList();
    }
}
