package com.combino.collections.list;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ExampleArrayList {

    public void printArrayList() {

        List<Person> people = new ArrayList<>();
        AbstractList<Person> personAbstractList = new Vector<>();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("1001");
        arrayList.add("1001");
        arrayList.add("1001");
        arrayList.add("1001");
        System.out.println(arrayList);
        System.out.println(arrayList.remove("1001"));
        System.out.println(arrayList);

        Person peter = new Person();
        peter.setId(1001);
        peter.setAge(46);
        peter.setEmail("mail@mail.com");
        peter.setMobileNumber(4696321478L);
        peter.setWeight(76.89);
        peter.setName("peter");
        people.add(peter);

        Person ramu = new Person();
        ramu.setId(1002);
        ramu.setAge(46);
        ramu.setEmail("ramu@mail.com");
        ramu.setMobileNumber(4696321478L);
        ramu.setWeight(66.89);
        ramu.setName("ramu");
        people.add(ramu);

        Person suresh = new Person();
        suresh.setId(1003);
        suresh.setAge(46);
        suresh.setEmail("suresh@mail.com");
        suresh.setMobileNumber(4696555478L);
        suresh.setWeight(96.89);
        suresh.setName("suresh");
        people.add(suresh);

        System.out.println(people);

    }
}
