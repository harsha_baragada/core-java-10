package com.combino.collections.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

public class ExampleLinkedList {
    LinkedList<String> sequentialList = new LinkedList<>();
    ArrayList<String> cities = new ArrayList<>();


    public void printLinkedList() {
        sequentialList.add("Mumbai");
        sequentialList.add("Delhi");
        sequentialList.add("Hyderabad");
        sequentialList.add("Chennai");
        sequentialList.add("KOlkata");
        System.out.println(sequentialList);
        System.out.println(sequentialList.getFirst());
        System.out.println(sequentialList.getLast());
        System.out.println(sequentialList.removeFirst());
        System.out.println(sequentialList);
        System.out.println(sequentialList.removeLast());
        System.out.println(sequentialList);
        System.out.println(sequentialList.removeLast());
        System.out.println(sequentialList);

        cities.add("London");
        cities.add("Utrecht");
        cities.add("Amsterdam");
        cities.add("Frankfurt");
        cities.add("Paris");
        System.out.println(sequentialList.addAll(cities));
        System.out.println(sequentialList);


    }

}
