package com.combino.date;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class ExampleDate {

    public void printDate() throws InterruptedException {
        Date date = new Date();
        System.out.println(date);
        Thread.sleep(4000);
        Date later = new Date();
        System.out.println(later);

        System.out.println(date.after(later));
        System.out.println(date.before(later));
        System.out.println(date.getTime());
        System.out.println(later.getTime());
        date.setTime(1642037826338L);
        System.out.println(date);
        System.out.println(date.equals(later));
        System.out.println(date.hashCode());
        System.out.println(date.toString() instanceof String);
    }


    public void printSimpleDateFormats(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E dd-MM-yyyy hh:mm:ss a zzz");
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("E MM-dd-yyyy hh:mm:ss a zzz");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("E dd MMM, yyyy hh:mm:ss  zzz");
        SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("E dd/MMM/yy hh:mm:ss a zzz");
        System.out.println(simpleDateFormat.format(date));
        System.out.println(simpleDateFormat1.format(date));
        System.out.println(simpleDateFormat2.format(date));
        System.out.println(simpleDateFormat3.format(date));

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        System.out.println(gregorianCalendar);
        System.out.println(gregorianCalendar.getTime());

        GregorianCalendar centralGregorianCalendar= new GregorianCalendar(TimeZone.getTimeZone("GMT+10"));
        System.out.println(centralGregorianCalendar.getTimeInMillis());
        System.out.println(centralGregorianCalendar.isLeapYear(2024));


    }
}
