package com.combino.date;

public class Tester {
    public static void main(String[] args) throws InterruptedException {

        ExampleDate date = new ExampleDate();
        date.printDate();
        date.printSimpleDateFormats();
    }
}
