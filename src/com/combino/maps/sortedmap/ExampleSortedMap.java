package com.combino.maps.sortedmap;

import java.util.SortedMap;
import java.util.TreeMap;

public class ExampleSortedMap {

    SortedMap sortedMap = new TreeMap();

    public void printSortedMap(){
        sortedMap.put(1001,"Apple");
        sortedMap.put(1004,"Banana");
        sortedMap.put(1008,"Mango");
        sortedMap.put(1002,"Orange");
        sortedMap.put(1005,"Strawberry");
        System.out.println(sortedMap);
    }
}
