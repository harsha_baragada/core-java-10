package com.combino.maps.LinkedHashMaps;

import java.util.LinkedHashMap;

public class ExampleLinkedHashMap {

    public void printLinkedMap(){
        LinkedHashMap<Integer,String> linkedHashMap = new LinkedHashMap<>();

        linkedHashMap.put(1001,"Apple");
        linkedHashMap.put(1004,"Banana");
        linkedHashMap.put(1008,"Mango");
        linkedHashMap.put(1002,"Orange");
        linkedHashMap.put(1005,"Strawberry");
        System.out.println(linkedHashMap);

    }
}
