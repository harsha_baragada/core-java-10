package com.combino.maps;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class ExampleMaps {

    Map<Integer, String> map = new HashMap<>();

    public void printMap(){
        map.put(1001,"Zara");
        map.put(1002,"park");
        map.put(1003,"meta");
        map.put(1004,"max");
        map.put(1005,"eva");
        map.put(1006,"arva");
        System.out.println(map);
        System.out.println(map.size());
        System.out.println(map.isEmpty());
        System.out.println(map.containsKey(10010));
        System.out.println(map.containsValue("Zara"));
        System.out.println(map.get(1006));
        System.out.println(map.remove(1005));
        System.out.println(map);
        System.out.println(map.replace(1004,"max","Juji") );
        System.out.println(map);
    }
}
