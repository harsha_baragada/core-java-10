package com.combino.datastructures;

import java.util.Enumeration;
import java.util.Vector;

public class ExampleEnumeration {

    Enumeration<String> fruitsEnumeration;
    Vector<String> fruits = new Vector<>();

    public void printEnumeration() {

        fruits.add("Apple");
        fruits.add("Banana");
        fruits.add("Straw berry");
        fruits.add("Blue berry");
        fruits.add("Black berry");
        fruits.add("Red berry");
        fruits.add("Cran berry");
        fruitsEnumeration = fruits.elements();
        while (fruitsEnumeration.hasMoreElements()){
            System.out.println(fruitsEnumeration.nextElement());
        }

    }
}
