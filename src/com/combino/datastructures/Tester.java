package com.combino.datastructures;

public class Tester {
    public static void main(String[] args) {
        ExampleEnumeration enumeration = new ExampleEnumeration();
        enumeration.printEnumeration();

        ExampleStack exampleStack = new ExampleStack();
        exampleStack.printTheStackOfBooks();

        System.out.println("********************************************");
        ExampleDataStructures exampleDataStructures = new ExampleDataStructures();
        exampleDataStructures.printDictionary();
    }
}
