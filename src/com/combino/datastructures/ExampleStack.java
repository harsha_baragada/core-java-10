package com.combino.datastructures;

import java.util.Stack;

public class ExampleStack {
    Stack<String> books = new Stack<>();

    public void printTheStackOfBooks() {
        books.addElement("How to influence friends and people");
        books.addElement("Zero to one");
        books.addElement("$100 startup");
        books.addElement("Intelligent investor");
        books.addElement("Psychology of money");
        System.out.println(books);
        System.out.println(books.pop());
        System.out.println(books);
        System.out.println(books.pop());
        System.out.println(books);
        System.out.println(books.pop());
        System.out.println(books);
    }
}
