package com.combino.array;

import java.util.Arrays;

public class ExampleArrays {

    // this is the preferable way
    String[] desserts = {"Ice cream","Gulab jamoon","Rosagulla","Laddu","Mango lassi"};
    // works but not preferable
    int ids[]={1001,20205,524,6546,6565,65465,6352,6354,654,65789789,4564,878,52,5564685,683};

    public void playWithArrays(){
        System.out.println(ids);
        for (String dessert: desserts
             ) {
            System.out.println(dessert);
        }
        for (int id:ids
             ) {
            System.out.println(id);
        }
        System.out.println(Arrays.binarySearch(ids,8563));
    }

}
