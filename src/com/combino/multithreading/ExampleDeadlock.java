package com.combino.multithreading;

public class ExampleDeadlock {

    public static Object Lock1 = new Object();
    public static Object Lock2 = new Object();

    public static void main(String[] args) {
        ThreadDemo1 demo1 = new ThreadDemo1();
        ThreadDemo2 demo2 = new ThreadDemo2();
        demo1.start();
        demo2.start();
    }

    private static class ThreadDemo1 extends Thread {
        public void run() {
            synchronized (Lock1) {
                System.out.println("Thread 1 is holding lock 1");
                try {
                    Thread.sleep(10);
                } catch (Exception e) {

                }
                System.out.println("Thread 1 is waiting for Lock2");
                synchronized (Lock2) {
                    System.out.println("Thread 1 is holding lock 1 & 2");
                }
            }

        }

    }

    private static class ThreadDemo2 extends Thread {
        public void run() {
            synchronized (Lock2) {
                System.out.println("Thread 2 is holding lock 2");
                try {
                    Thread.sleep(10);
                } catch (Exception e) {

                }
                System.out.println("Thread 2 is waiting for Lock1");
                synchronized (Lock1) {
                    System.out.println("Thread 2 is holding lock 1 & 2");
                }
            }

        }

    }

}
