package com.combino.multithreading;

public class Tester {
    public static void main(String[] args) {
      /*  ExampleMultiThreading2 multithreading1 = new ExampleMultiThreading2("Thread-1");
        multithreading1.start();

        ExampleMultiThreading2 multithreading2 = new ExampleMultiThreading2("Thread-2");
        multithreading2.start();

        ExampleMultiThreading2 multithreading3 = new ExampleMultiThreading2("Thread-3");
        multithreading3.start();*/

        ExampleSynchronization exampleSynchronization = new ExampleSynchronization();
        DemoThread thread1 = new DemoThread("Thread--1",exampleSynchronization);

        DemoThread thread2 = new DemoThread("Thread--2",exampleSynchronization);
        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        }catch (Exception  e){

        }
    }
}
