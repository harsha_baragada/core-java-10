package com.combino.multithreading;



public class ExampleMultithreading implements Runnable {



    private Thread thread;
    private String threadName;

    public ExampleMultithreading(String threadName) {
        this.threadName = threadName;
    }

    @Override
    public void run() {
        System.out.println("Running " + threadName);
        try {
            for (int i = 0; i < 4; i++) {
                System.out.println("Thread " + threadName + " " + i);
                Thread.sleep(50);
            }

        } catch (InterruptedException e) {
            System.out.println("There is an exception occurred ");
        }

    }

    public void start(){
        System.out.println("The thread "+threadName+" is starting");
        if(thread == null){
            thread = new Thread(this, threadName);
            thread.start();
        }
    }


}
