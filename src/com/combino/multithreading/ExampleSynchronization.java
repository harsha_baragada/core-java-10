package com.combino.multithreading;

public class ExampleSynchronization {

    public void printCount() {
        try {
            for (int i = 0; i < 5; i++) {
                System.out.println("Counter ======" + i);
            }
        } catch (Exception e) {
            System.out.println("Thread exception ");
        }

    }
}

class DemoThread extends Thread {
    private Thread thread;
    private String threadName;
    private ExampleSynchronization exampleSynchronization;

    public DemoThread(String threadName, ExampleSynchronization exampleSynchronization) {
        this.threadName = threadName;
        this.exampleSynchronization = exampleSynchronization;
    }

    public void run() {

        synchronized (exampleSynchronization){
            exampleSynchronization.printCount();
        }
        System.out.println("Thread " + threadName + " existing");
    }

    public void start() {
        System.out.println("Starting " + threadName + " starting");
        if (thread == null) {
            thread = new Thread(this, threadName);
            thread.start();

        }
    }
}